let net = require('net');

function dd(n) {
  return n > 10 ? `${n}` : `0${n}`;
}

net.createServer((socket) => {
  let now = new Date();
  socket.write(`${now.getFullYear()}-${dd(now.getMonth() + 1)}-${dd(now.getDate())} ${dd(now.getHours())}:${dd(now.getMinutes())}\n`);
  socket.end();
}).listen(process.argv[2]);
