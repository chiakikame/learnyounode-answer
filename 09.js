let http = require("http");

let urls = process.argv.slice(2);
let slots = urls.map(() => null);

function getData(url, dataCallback) {
  let accum = '';

  return new Promise(function httpGetExec(resolve, reject) {
    http.get(url, (res) => {
      res.setEncoding('utf8');
      res.on('data', (data) => {
        accum += data;
      });
      res.on('end', () => {
        resolve(accum);
      });
      res.on('error', reject);
    }).on('error', reject);
  });
}

Promise.all(urls.map((url) => {
  return getData(url);
})).then((results) => {
  results.forEach((item) => console.log(item));
}, (err) => console.error);
