let http = require('http');
let fs = require('fs');

let filePath = process.argv[3];
let port = process.argv[2];

http.createServer((req, res) => {
  let readStream = fs.createReadStream(filePath);
  readStream.pipe(res);
}).listen(port);
