let http = require('http');
let url = require('url');

http.createServer((req, res) => {


  if (req.method === "GET") {
    let reqUrlInfo = url.parse(req.url, true);
    let timeString = reqUrlInfo.query['iso'];

    if (timeString) {
      let time = new Date(timeString);

      res.writeHead(200, { 'Content-Type': 'application/json' });
      if (reqUrlInfo.pathname === "/api/parsetime") {
        res.write(JSON.stringify({
          'hour': time.getHours(),
          'minute': time.getMinutes(),
          'second': time.getSeconds()
        }));
      } else if (reqUrlInfo.pathname === "/api/unixtime") {
        res.write(JSON.stringify({
          'unixtime': time.getTime()
        }));
      }

    }
    res.end();
  }
}).listen(process.argv[2]);
