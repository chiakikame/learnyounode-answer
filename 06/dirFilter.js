let fs = require("fs");
let path = require("path");

module.exports = function extFilter(dir, ext, callback) {
  fs.readdir(dir, (err, itemList) => {
      if (err) {
        callback(err, null);
        return;
      } else {
        callback(null, itemList.filter((item) => path.extname(item) === "." + ext ));
      }
  });
}
