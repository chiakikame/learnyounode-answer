let dirFilter = require("./dirFilter");

dirFilter(process.argv[2], process.argv[3], (err, items) => {
  if (err) {
    throw err;
  } else {
    items.forEach((item) => console.log(item));
  }
});
