let fs = require("fs");
let path = require("path");

let ext = '.' + process.argv[3];

fs.readdir(process.argv[2], (err, itemList) => {
  if (err) {
    throw err;
  } else {
    itemList.filter((item) => path.extname(item) === ext).forEach((item) => {
      console.log(item);
    });
  }
})
