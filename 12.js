let http = require("http");
let map = require("through2-map");

http.createServer((req, res) => {
  if (req.method === "POST") {
    req.pipe( map((chunk) => chunk.toString().toUpperCase()) ).pipe(res);
  }
  // All other methods are just ignored
}).listen(process.argv[2]);
