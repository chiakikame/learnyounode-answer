let http = require("http");

http.get(process.argv[2], (res) => {
  res.setEncoding('utf8');
  let accum = '';
  res.on('data', (data) => {
    accum += data;
  });
  res.on('end', () => {
    console.log(accum.length);
    console.log(accum);
  });
  res.on('error', console.error);
}).on('error', console.error);
